//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"

#include <map>
#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INTEGER, BOOLEAN};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string


map<string, enum TYPES> DeclaredVariables;	// Store declared variables and their types
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// Declaration := Ident {"," Ident} Type
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement|IfStatement|WhileStatement|ForStatement|BlockStatement|DisplayStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
enum TYPES Identifier(void){
	enum TYPES type;
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	type=DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;

}

enum TYPES Number(void){
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	return INTEGER;
}

enum TYPES Expression(void);			// Called by Term() and calls Term()

enum TYPES Factor(void){
	enum TYPES type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			type = Number();
	     	else
				if(current==ID)
					type = Identifier();
				else
					Error("'(' ou chiffre ou lettre attendue");
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
enum TYPES Term(void){
	TYPES type1, type2;
	OPMUL mulop;
	type1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		if(type2!=type1)
			Error("types incompatibles dans l'expression");
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				if(type2!=BOOLEAN)
					Error("le type doit être BOOLEAN dans l'expression");
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(type2!=INTEGER)
					Error("le type doit être INTEGER dans l'expression");
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				if(type2!=INTEGER)
					Error("le type doit être INTEGER dans l'expression");
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				if(type2!=INTEGER)
					Error("le type doit être INTEGER dans l'expression");
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;

}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
enum TYPES SimpleExpression(void){
	enum TYPES type;
	OPADD adop;
	type = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		if(type != Term()) {
			Error("addition de types incompatibles");
		}
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return type;

}

enum TYPES Type(void){
	if(current!=KEYWORD)
		Error("type attendu");
	if(strcmp(lexer->YYText(),"BOOLEAN")==0){
		current=(TOKEN) lexer->yylex();
		return BOOLEAN;
	}	
	else if(strcmp(lexer->YYText(),"INTEGER")==0){
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
	else
		Error("type inconnu");
	
}


//Declaration := Ident { "," Ident} Type 
void Declaration(){

	set<string> idents;
	enum TYPES type;

	if(current!=ID)
		Error("Un identificater était attendu");
	idents.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		idents.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	type=Type();
	for (set<string>::iterator it=idents.begin(); it!=idents.end(); ++it){
	    cout << *it << ":\t.quad 0"<<endl;
            DeclaredVariables[*it]=type;
	}

}

// DeclarationPart := "[" Declaration {";" Declaration } "." ]"
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	current=(TOKEN) lexer->yylex();
	
	Declaration();

	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Declaration();
	}

	if(current!=DOT){
		Error("'.' attendu");
	}

	current=(TOKEN) lexer->yylex();
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}


// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
enum TYPES Expression(void){
	enum TYPES type;
	OPREL oprel;
	type = SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		if(type != SimpleExpression()){
			Error("type d'Epression incompatibles");	
		}
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOLEAN;
	}
	return type;
}

void Statement(void);			//calls IfStatement and called by IfStatement

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	enum TYPES type;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type=DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	if(type != Expression()){
		Error("type de variable et d'expression incompatibles");
	}
	cout << "\tpop "<<variable<<endl;
}

//IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){

	unsigned long TagLoc = TagNumber+1;
	if(strcmp(lexer->YYText(),"IF")!=0){
		Error("'IF' attendu"); // keyword expected
	}
	cout << "If" << TagLoc <<":\n";

	current=(TOKEN) lexer->yylex();
	if(Expression() != BOOLEAN){
		Error("IF ne fonctionne qu'avec des booléen");
	}

	cout << "\tpop %rax" << endl;
	cout << "\tcmpl $0xFFFFFFFFFFFFFFFF, %rax" << endl;
	cout << "\tjne Else"<< TagLoc << endl;

	if(strcmp(lexer->YYText(),"THEN")!=0){
		Error("'THEN' attendu");
	}

	current=(TOKEN) lexer->yylex();
	Statement();

	cout << "\tjmp EndIf"<< TagLoc << endl;
	cout << "Else"<< TagLoc << ":"<< endl;

	if(current==KEYWORD){
		if(strcmp(lexer->YYText(),"ELSE")==0){
			current=(TOKEN) lexer->yylex();
			Statement();
		}
	}

	cout << "EndIf"<< TagLoc << ":"<< endl;
}

//WhileStatement := "While" Expression "DO" Statement
void WhileStatement(void){

	unsigned long TagLoc = TagNumber+1;
	if(strcmp(lexer->YYText(),"WHILE")!=0){
		Error("'WHILE' attendu"); // keyword expected
	}

	cout << "While" << TagLoc << ":\n";

	current=(TOKEN) lexer->yylex();
	if(Expression() != BOOLEAN){
		Error("WHILE ne fonctionne qu'avec des booléen");
	}

	cout << "\tpop %rax" << endl;
	cout << "\tcmpl $0xFFFFFFFFFFFFFFFF, %rax" << endl;
	cout << "\tjne EndWhile"<< TagLoc << endl;

	if (strcmp(lexer->YYText(),"DO")!=0){
		Error("'Do' attendu"); // 'DO' expected
	}

	current=(TOKEN) lexer->yylex();
	Statement();

	cout << "\tjmp While"<< TagLoc << endl;
	cout <<"EndWhile"<<TagLoc << ":\n";

}
//ForStatement := "FOR" AssignementStatement "TO" Expression "DO" Statement
void ForStatement(void){

	unsigned long TagLoc = TagNumber+1;
	if(strcmp(lexer->YYText(),"FOR")!=0){
		Error("'FOR' attendu"); // keyword expected
	}

	current=(TOKEN) lexer->yylex();
	AssignementStatement();

	cout << "For" << TagLoc << ":\n";

	if(strcmp(lexer->YYText(),"TO")!=0){
		Error("'TO' attendu"); // keyword expected
	}

	current=(TOKEN) lexer->yylex();
	Expression();

	if(strcmp(lexer->YYText(),"DO")!=0){
		Error("'DO' attendu"); // keyword expected
	}

	cout << "\tpop %rax" << endl;
	cout << "\tcmpl $0xFFFFFFFFFFFFFFFF, %rax" << endl;
	cout << "\tje EndFor"<< TagLoc << endl;	

	current=(TOKEN) lexer->yylex();
	Statement();

	cout << "\tjmp For" << TagLoc<<endl;
	cout << "EnfFor"<< TagLoc << ":\n";

}

//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void){
	unsigned long TagLoc = TagNumber+1;
	if(strcmp(lexer->YYText(),"BEGIN")!=0){
		Error("'BEGIN' attendu"); // keyword expected
	}

	cout << "Block" << TagLoc << ":\n"; 

	current=(TOKEN) lexer->yylex();
	Statement();
	while (current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}

	if(strcmp(lexer->YYText(),"END")!=0){
		Error("'END' attendu"); // keyword expected
	}
	current=(TOKEN) lexer->yylex();

	cout << "EndBlock" << TagLoc << ":\n"; 

}


//DisplayStatement := "DISPLAY" Expression
void DisplayStatement(void){
	enum TYPES type;
	unsigned long long tag=++TagNumber;
	current=(TOKEN) lexer->yylex();
	type=Expression();
	if(type==INTEGER){
		cout << "\tpop %rdx\t# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString1, %rsi\t# \"%llu\\n\""<<endl;
	}
	else
		if(type==BOOLEAN){
			cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
			cout << "\tcmpq $0, %rdx"<<endl;
			cout << "\tje False"<<tag<<endl;
			cout << "\tmovq $TrueString, %rsi\t# \"TRUE\\n\""<<endl;
			cout << "\tjmp Next"<<tag<<endl;
			cout << "False"<<tag<<":"<<endl;
			cout << "\tmovq $FalseString, %rsi\t# \"FALSE\\n\""<<endl;
			cout << "Next"<<tag<<":"<<endl;
		}
		else
			Error("DISPLAY ne fonctionne que pour les nombres entiers");
	cout << "\tmovl	$1, %edi"<<endl;
	cout << "\tmovl	$0, %eax"<<endl;
	cout << "\tcall	__printf_chk@PLT"<<endl;

}

// Statement := AssignementStatement|IfStatement|WhileStatement|ForStatement|BlockStatement|DisplayStatement
void Statement(void){
	switch(current){
			case ID:
				AssignementStatement();
				break;
			case KEYWORD:
				if (strcmp(lexer->YYText(),"IF")==0){
					IfStatement();
				}
				else if (strcmp(lexer->YYText(),"WHILE")==0){
					WhileStatement();
				}

				else if (strcmp(lexer->YYText(),"FOR")==0){
					ForStatement();
				}
				else if(strcmp(lexer->YYText(),"BEGIN")==0){
					BlockStatement();
				}
				else if(strcmp(lexer->YYText(),"DISPLAY")==0){
					DisplayStatement();
				}
				break;

			default :
				Error("caractère inatendu");
		}
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "TrueString:\t.string \"TRUE\\n\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\\n\"\t# used by printf to display the boolean value FALSE"<<endl; 
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}


}
		
			





